Trong cuộc sống, con người cũng có lúc ốm lúc đau. Chiếc máy lạnh cũng vậy, cũng có lúc nó gặp các sự cố cho dù là nhỏ hay lớn thì nó vẫn đem lại cho chúng ta một cái cảm giác không yên tâm và không an toàn với thiết bị tiện nghi này.

Do đó, ở bài viết này chúng tôi cung cấp cho người đọc những kiến thức cơ bản nhất để xác định các nguyên nhân và hiện tượng khi máy điều hòa không khí bị sự cố và các cách khắc phụ cơ bản để khác hàng có thể từ đó có biện pháp cụ thể để triển khai nhằm khắc phục được phần nào hay tối đa sự cố.

http://www.dichvusuamaylanh.net/sua-may-lanh-quan-binh-thanh-1

Máy điều hòa không khí là một hệ thống kín và gas lạnh bên trong máy là loại hóa chất rất bền không bị phân hủy trong điều kiện hoạt động của máy nên không có hiện tượng hao hụt gas. Máy chỉ thiếu gas, hết gas trong trường hợp bị rò rỉ, xì trên đường ống, tại các van, các chỗ đấu nối ống bằng rắc-co...hay trong quá trình lắp mới người lắp đặt không kiểm tra và nạp đủ gas.

Khi máy bị thiếu gas hoặc hết gas sẽ có một số hiện tượng sau:

Máy không lạnh, kém lạnh.
Bám tuyết ngay van ống nhỏ của dàn nóng.
Dòng điện hoạt động thấp hơn dòng định mức ghi trên máy.
Áp suất gas hút về máy nén thấp hơn áp làm việc bình thường (bình thường từ 65-75psi). Áp suất phía cao áp cũng thấp hơn bình thường.
Trong một số máy lạnh, khi bị thiếu gas board điều khiển sẽ tự động tắt máy sau khoảng 5-10 phút và báo lỗi trên dàn lạnh.
http://www.dichvusuamaylanh.net/sua-may-lanh-quan-tan-binh

2. Máy nén không chạy

Máy nén (Block) được xem là trái tim của máy ĐHKK, khi máy nén không chạy thì máy ĐHKK không lạnh. Một số nguyên nhân làm máy nén không chạy :

1. Mất nguồn cấp đến máy nén : do lỗ do board điều khiển, contactor không đóng, hở mạch.

2. Nhảy thermic bảo vệ máy nén : thường do hư tụ, quạt dàn nóng yếu hoặc hư, motor máy nén không quay.

3. Cháy một trong các cuộn dây động cơ bên trong, trường hợp này có thể dẫn tới nhảy CB nguồn.

http://www.dichvusuamaylanh.net/sua-may-lanh-quan-go-vap

3. Máy nén chạy ồn:

Khi máy lạnh của bạn bị hiện tượng này thì bạn thường nghe được tiếng ồn phát ra từ phía giàn nóng, tức là từ phía cục nóng thường đặt ngoài trời.

a. Nguyên nhân:
1. Dư gas.

2. Có chi tiết bên trong máy nén bị hư.

3. Có các bulong hay đinh vít bị lỏng

4. Chưa tháo các tấm vận chuyển

5. Có sự tiếp xúc của 1 ống này với ống khác hoặc vỏ máy
http://www.dichvusuamaylanh.net

b. PP KT sữa chữa:

1. Rút bớt lượng gas đã sạc bằng cách xả ga ra môi trường bằng khóa lục giác. Vị trí xả ra ngay tại đầu côn phía cuối của giàn nóng – cục nóng.

2. Thay máy nén bằng cách đi mua máy nén đúng mã số, thương hiệu, đúng công suất và thay thế hoặc nhờ tới chuyên viên thay thế.

3. Vặn chặt các bulông hay vis, kiểm tra xem máy nén có đúng với tình trạng như ban đầu hay không nhé.

4. Tháo các tấm vận chuyển nhằm để cho hệ máy đỡ va chạm và gây kêu.

5. Nắn thẳng hay cố định ống sao cho không tiếp xúc với ống hoặc các chi tiết kim loại khác. Kiểm tra xem mặt đế đặt máy nén có bị xiên, lũng hay bị cong làm cho máy nén bị xiên và đụng với thành của võ giàn nóng – cục nóng và gây nên kêu.Kiểm tra xem các buloong phía dưới đáy máy nén xem có lỏng hay không. Nếu lõng thì xiết vừa phải nhé. Không được xiết chặt các buloong đó nhé.

http://www.dichvusuamaylanh.net/sua-may-lanh-quan-phu-nhuan